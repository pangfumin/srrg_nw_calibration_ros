# SRRG NW Calibration Ros Package

This package allows to simoultaneously **calibrate** the intrinsics parameters of a mobile platform (_differential drive / omnidirectional_) and the relative pose
of as many 2d and 3d sensors as you have mounted on it. If you trust enough the odometry of the platform, you can still use this package to **calibrate** only the
relative poses of you sensors.

## Prerequisites

* [srrg_nw_calibration](https://gitlab.com/srrg-software/srrg_nw_calibration)

## Installation

Standard ros package installation

    git clone https://gitlab.com/srrg-software/srrg_nw_calibration_ros.git
    catkin_make --pkg srrg_nw_calibration_ros

## Example

Every node of the package `srrg_nw_calibration_ros` provides an help by typing `-h`. 
Usual command

### Case 1: You want to perform simultaneously intrinsics (joints) and extrinsics (sensor poses) calibration

    rosrun srrg_nw_calibration_ros new_world_calibration_ros_node -guess init_guess.txt

### Case 2: You want to perform only extrinsics (sensor poses) calibration given a good platform odometry

    rosrun srrg_nw_calibration_ros nw_odom_calibration_ros_node -guess init_guess.txt
    
### Initial Guess

An initial guess file has to be provided. An example of this file can be found in the folder `initial_guess`.
The `init_guess.txt` file contains the list of sensors to be calibrated, the relative tf to be read,
and the intial set of parameters the solver will use.

As an example, imagine to have 2 sensors, a 2D laser and a 3D camera mounted on your differential drive robot. In order to calibrate the sensors
you need two tracker, a 2d tracker for the laser, e.g. [srrg_scan_matcher](https://gitlab.com/srrg-software/srrg_scan_matcher_ros), and
a 3d tracker for the camera, e.g. [ORB_SLAM2](https://github.com/raulmur/ORB_SLAM2). The trackers publish a `tf` expressing the
pose of the relative sensors with respect to an origin. Let these tfs be `/odom` --> `/laser` and `/odom` --> `/camera`, I will write in my guess.txt file
the following:

#### ODOM_PARAMS kl kr baseline

* `ODOM_PARAMS -1.5 1.5 0.3`

Baseline is expressed in `meters`.

If you don't need to calibrate the intrinsics, simply omit this line.

#### SENSOR2_PARAMS x y theta

* `SENSOR2_PARAMS /odom /laser 0 0 0`

Where `theta` is expressed in `radians`.

Add one of this line for each 2d sensor you have.

#### SENSOR3_PARAMS x y z qx qy qz

* `SENSOR3_PARAMS /odom /camera 0 0 0.515 -0.47 0.52 -0.51`

Where `qx qy qz` are the imaginary part of a unit quaternion.

Add one of this line for each 2d sensor you have.

## Testing Dataset

A bag (with only tf recorder) of a **Hokuyo URG scanner**, and **3 Asus Xtion** mounted on a custom **differential drive** platform while performing an eight-shaped path.
The tf attached to the Laser Scanner is obtained by scan matching, while the 3 tfs attached to the cameras have been recorder with [ORB_SLAM2](https://github.com/raulmur/ORB_SLAM2) properly modified to publish the transforms.

Download: [dataset 8-shape-path](https://drive.google.com/file/d/0BxhfDMgREiwXMjY2X29OWjlEbUU/view?usp=sharing)

Run the bag and test the calibration:

    rosrun srrg_nw_calibration_ros new_world_calibration_ros_node -guess init_guess.txt

## Unsupervised Calibration
The node `srrg_nw_calibration_ros_auto_node` provides an **autonomous way** to calibrate your platform.

    rosrun srrg_nw_calibration_ros new_world_calibration_ros_auto_node -guess init_guess.txt


## Authors

* Bartolomeo Della Corte
* Giorgio Grisetti
* Maurilio Di Cicco

## Related Publications

* M.Di Cicco and B.Della Corte and G.Grisetti. "[**Unsupervised calibration of wheeled mobile platforms**](http://ieeexplore.ieee.org/document/7487631/)", In Proc. of the IEEE International Conference on Robotics and Automation (ICRA), Stockholm, 2016.
